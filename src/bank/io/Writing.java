/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.io;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import bank.data.*;

/*
 * Interface for save customer information
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public interface Writing {
	void saveCustomerStateToFile(String path, List<Customer> customers) throws FileNotFoundException, UnsupportedEncodingException;
}
