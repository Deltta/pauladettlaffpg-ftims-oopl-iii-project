/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.io;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import bank.data.*;
/*
 * Implementation of write interface, in this case it will write to txt file
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public class CustomerWriter implements Writing {

	public CustomerWriter() {
	}

	@Override
	public void saveCustomerStateToFile(String path, List<Customer> customers) throws FileNotFoundException, UnsupportedEncodingException {
		//Printer will override file if already exists 
		PrintWriter writer = new PrintWriter(path, "UTF-8");
		for(Customer cust : customers) {
			writer.println(cust.showCustomer());
			for(Product prod : cust.getAllProducts()) {
				writer.println(prod.displayProduct());
			}
		}
		writer.close();
	}

}
