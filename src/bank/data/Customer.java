/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.data;

import java.util.ArrayList;
import java.util.List;

/*
 * Model class for single customer
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public class Customer {
	
	public static int customerNumber = 1;
	
	private List<Product> products;
	private String name;
	private String lastName;
	private int id;

	public Customer(String name, String lastName) {
		this.name = name;
		this.lastName = lastName;
		products = new ArrayList<Product>();
		id = customerNumber;
		customerNumber += 1;
	}

	public List<Product> getAllProducts() {
		return products;
	}
	
	public String showCustomer() {
		return String.format("Id: %d Name: %s Lastname: %s with %d Products",
				id, name, lastName, products.size());
	}
	
	public String getFullname() {
		return name + " " + lastName;
	}

	public void addProduct(Product product) {
		products.add(product);
	}
}
