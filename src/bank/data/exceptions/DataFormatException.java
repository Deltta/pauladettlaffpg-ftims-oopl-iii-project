/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.data.exceptions;

/*
 * Class is responsible for date format errors
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public class DataFormatException extends Exception {
	
	public DataFormatException(String format) {
		//Just invoke base class but with prepared error message
		super("Wrong date format, it should be " + format);
	}
}
