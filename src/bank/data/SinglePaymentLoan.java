/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.data;

/*
 * Class is a extend of loan to final product
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public class SinglePaymentLoan extends Loan {

	public SinglePaymentLoan(float loanAmount) {
		super(loanAmount);
		this.name = "Single Payment Loan";
	}

	@Override
	protected void setupPayments() {
		payments.put(1, this.getPaymentValue(1));
	}

}
