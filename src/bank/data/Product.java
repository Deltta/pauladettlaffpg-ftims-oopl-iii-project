/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import bank.data.exceptions.DataFormatException;

/*
 * Base class for every single product, store only base information
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public abstract class Product {
	
	private static String validDateFormat = "yyyy/MM/dd HH:mm:ss";

	protected float  value;
	protected String name;
	private Date date;
	
	public abstract String displayProduct();
	
	protected void setDate(String dateString) throws DataFormatException {
		DateFormat dateFormat = new SimpleDateFormat(validDateFormat);
		try {
			date = dateFormat.parse(dateString);
		} catch (ParseException e) {
			throw new DataFormatException(validDateFormat);
		}
	}
	
	protected String getDate() {
		DateFormat dateFormat = new SimpleDateFormat(validDateFormat);
	    return dateFormat.format(date);
	}
}
