/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.data;

/*
 * Class is a extend of loan to final product
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public class MultiPaymentLoan extends Loan {

	private int terms;
	
	public MultiPaymentLoan(float loanAmount, int terms) {
		super(loanAmount);
		this.terms = terms;
		this.name = "Multi Payment Loan";
	}	
	
	@Override
	protected void setupPayments() {
		for(int i = 1; i <= terms; i++) {
			payments.put(i, getPaymentValue(terms));
		}
	}
}
