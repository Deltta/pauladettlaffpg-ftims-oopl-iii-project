/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.data;

import java.util.HashMap;
import java.util.Map;

/*
 * Abstract common class for loan products
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public abstract class Loan extends Product {

	protected Map payments;
	private float loanAmount;
	private float repayAmount;
	
	/**
	 * Protected construtor which is used during build of inherit objects
	 */
	protected Loan(float loanAmount) {
		this.loanAmount = loanAmount;
		payments = new HashMap<Integer, Float>();
	}
	
	public void calculateLoan(float percentage) {
		setRepayAmount(loanAmount * percentage / 100);
		setupPayments();
	}
	
	@Override
	public String displayProduct() {
		return String.format("%s\n\tAmount: %f, Repay value: %f, Terms: %d", 
				name, loanAmount, repayAmount, payments.size());
	}
	
	protected void setRepayAmount(float additionalAmount) {
		repayAmount = loanAmount + additionalAmount;
	}
	
	protected float getPaymentValue(int terms) {
		return repayAmount / terms;
	}
	
	protected abstract void setupPayments();
}
