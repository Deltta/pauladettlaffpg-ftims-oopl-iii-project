package bank.enums;

public enum ConsoleState {
	Exit,	
	Customer,
	Product,
	Save,
	Menu
}
