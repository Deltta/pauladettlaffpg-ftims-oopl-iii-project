/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.customers;

import java.util.List;
import bank.data.Customer;

/*
 * Interface which be used to store customer data
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public interface CustomerStoring {
	List<Customer> getAllCustomers();
	boolean addCustomer(Customer newCustomer);
	Customer getCustomerById(int id);
}
