/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank.customers;

import java.util.ArrayList;
import java.util.List;
import bank.data.Customer;

/*
 * Class is implementation storing interface used to store customer data in memory
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public class CustomerStorage implements CustomerStoring {

	private List<Customer> customers;
	
	public CustomerStorage() {
		customers = new ArrayList<Customer>();
	}

	@Override
	public List<Customer> getAllCustomers() {
		return customers;
	}

	@Override
	public boolean addCustomer(Customer newCustomer) {
		customers.add(newCustomer);
		return true;
	}

	@Override
	public Customer getCustomerById(int id) {
		return customers.get(id);
	}

}
