/*
 * Paulina Dettlaff
 * 
 * Copyright info
 */
package bank;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.InputMismatchException;
import java.util.Scanner;

import bank.customers.*;
import bank.data.*;
import bank.enums.*;
import bank.io.*;

/*
 * Main class which is used on start application. In this case this is console application. This is something like singleton
 * 
 * @Version: 1.0 * 
 * @Author: Paula
 */
public class Main {
	
	/** Instance of class with that Writing interface will be used to save results */
	private static Writing writer;	
	private static CustomerStoring customerStorage;
	private static Customer selectedCustomer;

	/**
	 * Main method which is also our main loop, program will close when this loop ends
	 * @param args not used by me
	 */
	public static void main(String[] args) {
		init();
		
		Scanner input = new Scanner(System.in);
		ConsoleState state = ConsoleState.Menu;
		
		//Main loop
		while(state != ConsoleState.Exit) {		
			switch(state) {
			case Menu:
				displayMenuMessage();
				state = ConsoleState.values()[getInt(input, 5)];
				break;
			case Customer:
				displayCustomerMessage();
				state = customerHandler(ConsoleKeys.values()[getInt(input, 5)], input);
				break;
			case Product:
				displayProductMessage();
				state = productHandler(ConsoleKeys.values()[getInt(input, 5)], input);
				break;
			case Save:
				try {
					System.out.println("Enter file name:");
					writer.saveCustomerStateToFile(input.next(), customerStorage.getAllCustomers());
				} catch (FileNotFoundException e) {
					System.out.println("File not found");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				state = ConsoleState.Menu;
			default:
				break;				
			}			
		}		
		input.close();
	}
	
	/**
	 * Init class - here you can just replace used implementation of interfaces
	 */
	private static void init() {
		selectedCustomer = null;		
		customerStorage = new CustomerStorage();
		writer = new CustomerWriter();
	}
	
	/**
	 * Helper method to get correct number from input
	 * @param input 
	 * @param modulo - this number is used to limit number
	 * @return int number less than provided modulo
	 */
	private static int getInt(Scanner input, int modulo) {
		while(true) {
			if(input.hasNextInt()) {
				int out = input.nextInt();
				if(out > 0 && out < modulo) {
					return out;
				}
				System.out.println("Wrong key");
				continue;
			}
			input.next();
			System.out.println("Wrong key");
		}
	}
	
	private static ConsoleState customerHandler(ConsoleKeys key, Scanner input) {
		switch(key) {
		case Exit:
			return ConsoleState.Menu;
		case Add:
			String name;
			String lastName;
			System.out.flush();
			System.out.println("Enter name:");
			name = input.next();
			System.out.println("Enter lastname:");
			lastName = input.next();
			customerStorage.addCustomer(new Customer(name, lastName));
			break;
		case Select:
			if(customerStorage.getAllCustomers().isEmpty()) {
				System.out.println("No customers!");
				break;
			}
			System.out.println("Select customer by id:");
			selectedCustomer = customerStorage.getCustomerById(input.nextInt() - 1);
			return ConsoleState.Menu;
		}
		return ConsoleState.Customer;
	}
	
	private static ConsoleState productHandler(ConsoleKeys key, Scanner input) {
		switch(key) {
		case Exit:
			return ConsoleState.Menu;
		case Add:
			Product product = null;
			System.out.flush();
			System.out.println("1 for Single payment loan");
			System.out.println("2 for Multi payment loan");
			System.out.println("\nSelect type:");
			switch(getInt(input, 2)) {
			case 1:
				System.out.println("\nChoose loan amount:");
				product = new SinglePaymentLoan(input.nextFloat());
				System.out.println("\nProvide percentage:");
				((Loan)product).calculateLoan(input.nextFloat());
				break;
			case 2:
				System.out.println("\nChoose loan amount, then number of terms:");
				product = new MultiPaymentLoan(input.nextFloat(), input.nextInt());
				System.out.println("\nProvide percentage:");
				((Loan)product).calculateLoan(input.nextFloat());
				break;
			}
			if(product != null) { 
				selectedCustomer.addProduct(product);			
			}
			break;
		}
		return ConsoleState.Product;
	}

	private static void displayMenuMessage() {
		System.out.flush(); 			
		System.out.println("What you want to do?");
		if(selectedCustomer != null) {
			System.out.println("Customer: " + selectedCustomer.getFullname());
		}
		System.out.println("\n0 to Exit");
		System.out.println("1 to Customers");
		System.out.println("2 to Products");
		System.out.println("3 to Save to file");
	}
	
	private static void displayCustomerMessage()	{
		System.out.flush(); 			
		System.out.println("CUSTOMER MENU:");
		showAllCustomers();
		System.out.println("\n0 to Back");
		System.out.println("1 to Create New");
		System.out.println("2 to Select");
	}	

	private static void displayProductMessage()	{
		System.out.flush(); 			
		System.out.println("PRODUCT MENU:");
		showAllProduct();
		System.out.println("\n0 to Back");
		System.out.println("1 to Get Loan");
	}
	
	private static void showAllCustomers() {
		System.out.println("\n");
		for(Customer cust : customerStorage.getAllCustomers()) {
			System.out.println(cust.showCustomer());
		}
	}
	
	private static void showAllProduct() {
		System.out.println("\n");
		if(selectedCustomer != null) {
			for(Product prod : selectedCustomer.getAllProducts()) {
				System.out.println(prod.displayProduct());
			}
		}		
	}
}
